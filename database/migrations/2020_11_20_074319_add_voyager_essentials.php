<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVoyagerEssentials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('auth_user_id');
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->string('model')->nullable()->after('display_name');
            $table->string('scopes')->nullable()->after('model');
            $table->string('menus')->nullable()->after('scopes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
