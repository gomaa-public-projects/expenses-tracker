<?php

return [
    'verify_emails' => [
        'worker' => "App\Notifications\VerifyWorker",
    ],
    'reset_emails' => [
        'worker' => "App\Notifications\ResetWorkerPassword",
    ],

    "JWT_SECRET" => env("JWT_SECRET"),
];
