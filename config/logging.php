<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['main'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],

        //ET Logs
        'main' => [
            'driver' => 'daily',
            'path' => storage_path('logs/main/laravel.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'notifications' => [
            'driver' => 'daily',
            'path' => storage_path('logs/notifications/notifications.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'emails' => [
            'driver' => 'daily',
            'path' => storage_path('logs/emails/emails.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'listeners' => [
            'driver' => 'daily',
            'path' => storage_path('logs/listeners/listeners.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'authentication' => [
            'driver' => 'daily',
            'path' => storage_path('logs/authentication/authentication.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'guards' => [
            'driver' => 'daily',
            'path' => storage_path('logs/guards/guards.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'models' => [
            'driver' => 'daily',
            'path' => storage_path('logs/models/models.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],

        'graphQL_Queries' => [
            'driver' => 'daily',
            'path' => storage_path('logs/graphQL_Queries/graphQL_Queries.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],
        
        'graphQL_Resolvers' => [
            'driver' => 'daily',
            'path' => storage_path('logs/graphQL_Resolvers/graphQL_Resolvers.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],
        'payments' => [
            'driver' => 'daily',
            'path' => storage_path('logs/payments/payments.log'),
            'level' => 'debug',
            'permission' => 0775,
            'days' => 14,
        ],
    ],

];
