<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reports', function () {
    return view('report');
})->middleware('admin.auth');

Route::post('/pdf', "ExportController@pdf")->middleware('admin.auth');
Route::post('/excel', "ExportController@excel")->middleware('admin.auth');

Route::group(['prefix' => 'admin'], function () {
    VoyagerExtension::routes();
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('super.auth');
