$(document).ready(function () {
    $(`.${model_name} select.select2-ajax`).each(function() {
        $(this).select2({
            width: '100%',
            ajax: {
                url: $(this).data('get-items-route'),
                data: function (params) {
                     // get all the values of the other selects
                     let relationships = {};
                     let field = $(this).data('get-items-field');
                     $('select.select2-ajax').each(function (i)
                     {
                        let otherRelationName = $(this).data('get-items-field');
                        if (otherRelationName === field) {
                            console.log("dont open this:",field);
                            return;
                        }

                        let relationshipName="";
                        if(otherRelationName.includes("belongstomany")){
                            relationshipName = getNameBetween("belongstomany","relationship",otherRelationName);
                        }
                        else if(otherRelationName.includes("belongsto")){
                            relationshipName = getNameBetween("belongsto","relationship",otherRelationName);
                        }

                        relationships[relationshipName] = $(this).val();
                     });

                     console.log(relationships);
                     // form an array and send it
                     let relationType ="";
                     if(field.includes("belongstomany")){
                         relationType = getNameBetween("belongstomany","relationship",field);
                     }
                     else if(field.includes("belongsto")){
                        relationType = getNameBetween("belongsto","relationship",field);
                    }
                     var query = {
                         search: params.term,
                         type: field,
                         page: params.page || 1,
                         relationships: relationships,
                         myType:relationType
                     }
                     return query;
                }
            }
        });

        $(this).on('select2:select',function(e){
            var data = e.params.data;
            if (data.id == '') {
                // "None" was selected. Clear all selected options
                $(this).val([]).trigger('change');
            } else {
                $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected','selected');
            }
        });

        $(this).on('select2:unselect',function(e){
            var data = e.params.data;
            $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected',false);
        });
    });

    $('select.select2-taggable').select2({
        width: '100%',
        tags: true,
        createTag: function(params) {
            var term = $.trim(params.term);

            if (term === '') {
                return null;
            }

            return {
                id: term,
                text: term,
                newTag: true
            }
        }
    }).on('select2:selecting', function(e) {
        var $el = $(this);
        var route = $el.data('route');
        var label = $el.data('label');
        var errorMessage = $el.data('error-message');
        var newTag = e.params.args.data.newTag;

        if (!newTag) return;

        $el.select2('close');

        $.post(route, {
            [label]: e.params.args.data.text,
            _tagging: true,
        }).done(function(data) {
            var newOption = new Option(e.params.args.data.text, data.data.id, false, true);
            $el.append(newOption).trigger('change');
        }).fail(function(error) {
            toastr.error(errorMessage);
        });

        return false;
    }).on('select2:select', function (e) {
        if (e.params.data.id == '') {
            // "None" was selected. Clear all selected options
            $(this).val([]).trigger('change');
        }
    });

    $('.match-height').matchHeight();

    $('.datatable').DataTable({
        "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>'
    });

    $(".side-menu .nav .dropdown").on('show.bs.collapse', function () {
        return $(".side-menu .nav .dropdown .collapse").collapse('hide');
    });

    $('.panel-collapse').on('hide.bs.collapse', function(e) {
        var target = $(e.target);
        if (!target.is('a')) {
            target = target.parent();
        }
        if (!target.hasClass('collapsed')) {
            return;
        }
        e.stopPropagation();
        e.preventDefault();
    });

    $(document).on('click', '.panel-heading a.panel-action[data-toggle="panel-collapse"]', function (e) {
        e.preventDefault();
        var $this = $(this);

        // Toggle Collapse
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.removeClass('voyager-angle-up').addClass('voyager-angle-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.removeClass('voyager-angle-down').addClass('voyager-angle-up');
        }
    });

    //Toggle fullscreen
    $(document).on('click', '.panel-heading a.panel-action[data-toggle="panel-fullscreen"]', function (e) {
        e.preventDefault();
        var $this = $(this);
        if (!$this.hasClass('voyager-resize-full')) {
            $this.removeClass('voyager-resize-small').addClass('voyager-resize-full');
        } else {
            $this.removeClass('voyager-resize-full').addClass('voyager-resize-small');
        }
        $this.closest('.panel').toggleClass('is-fullscreen');
    });

    $(`.${model_name} .datepicker`).datetimepicker();
    //$(`${model_name} .datepicker`).datetimepicker();

    // Save shortcut
    $(document).keydown(function (e) {
        if ((e.metaKey || e.ctrlKey) && e.keyCode == 83) { /*ctrl+s or command+s*/
            $(".btn.save").click();
            e.preventDefault();
            return false;
        }
    });

    /********** MARKDOWN EDITOR **********/

    $('textarea.simplemde').each(function () {
        var simplemde = new SimpleMDE({
            element: this,
        });
        simplemde.render();
    });

    /********** END MARKDOWN EDITOR **********/

    function getNameBetween(start,end,name){
        let parts = name.split('_');
        let startFound= false;
        let endFound = false;
        let modelName =[]; 
        parts.forEach((value)=>{
            if((value == start || start ==="") && !startFound) { 
                startFound = true;
                return;
            }
            if((value == end) && !endFound) { 
                endFound = true;
                return;
            }
            if(startFound && !endFound){
                modelName.push(value);
            }
        });
        return modelName.join("_");
    }
    
    $(`[name = ${model_name+"-close"}]`).on('click',(e)=>{
        // console.log(model_name," and ",parent_slug, " and ",current_model_slug);
        if($(e.currentTarget)[0].getAttribute("name") == model_name+"-close"){
            ajax=""
            if(family_tree[current_model_slug].ajax){
                ajax=`${parent_slug}-`;
                //$(`#${ajax}create-edit-modal`).css('display','none');
                $(`#${ajax}create-edit-modal`).modal('hide');
                current_model_slug = parent_slug;
            }
            if(parent_slug != ""){
                if(family_tree[current_model_slug] === undefined){
                    console.log("slug: "+current_model_slug+" is not in family tree: "+family_tree);
                }

                parent = family_tree[current_model_slug].parent;
                parentId = family_tree[current_model_slug].parentId;
                model_name = family_tree[current_model_slug].model_name;
                parent_slug = family_tree[current_model_slug].parent_slug;
            }
        }
    });

    //BackDrop Fix
    // $('.modal').on('show.bs.modal', function(event) {
    //     var idx = $('.modal:visible').length;
    //     $(this).css('z-index', 1040 + (10 * idx));
    // });
    // $('.modal').on('shown.bs.modal', function(event) {
    //     var idx = ($('.modal:visible').length) -1; // raise backdrop after animation.
    //     $('.modal-backdrop').not('.stacked').css('z-index', 1039 + (10 * idx));
    //     $('.modal-backdrop').not('.stacked').addClass('stacked');
    // });

    

    // $(document).on('show.bs.modal', '.modal', function () {
    //     if ($(".modal-backdrop").length > -1) {
    //         $(".modal-backdrop").not(':first').remove();
    //     }
    // });




    //Scrolling Fix
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

});
