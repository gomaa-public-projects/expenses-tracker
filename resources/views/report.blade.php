<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Reports Page</title>


        <link href="{{asset('css/reports.css')}}" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

        <!-- Include Bootstrap Datepicker -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
        <style>
            td{
                width:20%;
            }
        </style>

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @php
                $workers = App\Worker::all();
            @endphp

            <div class="content">

                <div class="table-wrapper">
                    <table class="fl-table">
                        <thead>
                            <tr>
                                <th>Worker</th>
                                <th>Company</th>
                                <th>Account</th>
                                <th>From</th>
                                <th>To</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <select name="workers" class="form-control" id="workers">
                                            @foreach ($workers as $worker)
                                                <option value="{{$worker->id}}">{{$worker->name}}</option>
                                            @endforeach
                                    </select>
                                    <span style="margin-right:5%;">select all</span> <input type="checkbox" id="workers_check">
                                </td>
                                <td>      
                                    <select name="companies" class="form-control" id="companies">
                                        
                                    </select>
                                    <span style="margin-right:5%;">select all</span><input type="checkbox" id="companies_check">
                                </td>
                                <td>
                                    <select name="accounts" class="form-control" id="accounts">
                                        
                                    </select>
                                    <span style="margin-right:5%;">select all</span><input type="checkbox" id="accounts_check">
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="datepicker_from">
                                </td>
                                <td>
                                     <input type="text" class="form-control" id="datepicker_to">
                                </td>
                            </tr>
                        <tbody>
                    </table>
                </div>

                <div class ="text-center">
                    <button id="search" class="btn btn-primary" style="height: 10vh; width: 30%; background-color: #324960; border-color: #324960; margin: 10px 70px 70px;"> Search </button>
                </div>
                

                <div id="exportpdf" class="table-wrapper">
                    <table class="fl-table">
                        <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Currency</th>
                            <th>Company</th>
                            <th>Account</th>
                            <th>Worker</th>
                            <th>Exchange Rate</th>
                            <th>Amount (EGP)</th>
                            <th>Creation Date</th>
                        </tr>
                        </thead>
                        <tbody id="transactions">

                        <tbody>
                    </table>
                </div>
                <div class ="text-center">
                    <form action="/pdf" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <input id="pdfdata" hidden name=pdfdata>
                        <button id="pdf" class="btn btn-primary" style="height: 10vh; width: 20%; background-color: #F40F02; border-color: #F40F02; margin: 10px 70px 70px;"> PDF </button>
                    </form>

                    <form action="/excel" method="POST" style="display:inline">
                        {{ csrf_field() }}
                        <input id="exceldata" hidden name=exceldata>
                        <button id="excel" class="btn btn-primary" style="height: 10vh; width: 20%; background-color: #1D6F42; border-color: #1D6F42; margin: 10px 70px 70px;"> EXCEL </button>
                    </form>
                    
                </div>
            </div>
        </div>
        <script type="text/javascript">

            $(window).load(function() {
                callCompanyGraphQL($('#workers').val());
                callTransactionsGraphQL();
            });
            $('#datepicker_from').datepicker({
                format: 'yyyy-mm-dd',
                useCurrent: false
            }).datepicker("setDate", new Date());
            $('#datepicker_to').datepicker({
                format: 'yyyy-mm-dd',
                useCurrent: false
            }).datepicker("setDate", new Date());

           $('#companies').change(function(){
               callAccountGraphQL(this.value);
               callTransactionsGraphQL();
            });
            $('#workers').change(function(){
                callCompanyGraphQL(this.value);
                callTransactionsGraphQL();
            });
            $('#accounts').change(function(){
                callTransactionsGraphQL();
            });
            $('#datepicker_from').change(function(){
                callTransactionsGraphQL();
            });
            $('#datepicker_to').change(function(){
                callTransactionsGraphQL();
            });

            $('#workers').focus(function(){
                callWorkerGraphQL();
            });

            $('#search').click(function(){
                callTransactionsGraphQL();
            });

            $("#workers_check").change(function() {
                if(this.checked) {
                     $('#workers').prop("disabled", true);
                     return;
                }
                $('#workers').removeAttr('disabled');
            });

            $("#companies_check").change(function() {
                if(this.checked) {
                     $('#companies').prop('disabled', true);
                     return;
                }
                $('#companies').prop('disabled', false);
            });

            $("#accounts_check").change(function() {
                if(this.checked) {
                     $('#accounts').prop('disabled', true);
                     return;
                }
                $('#accounts').prop('disabled', false);
            });

            function callCompanyGraphQL(value)
            {
                 $.ajax({
                    type: 'POST', 
                    url: '/graphql',
                    data:{query:`query{companies(hasWorkers:{column:WORKER_ID,value:${value}}){id,name}}`},
                    success: function (data) {
                        console.log(data);
                        var companies = data.data.companies;
                        $('#companies').html("");
                        html="";
                        for (i = 0; i < companies.length; i++) {
                            company = companies[i];
                            html+=`<option value=${company.id}>${company.name}</option>`;
                        }
                        $('#companies').html(html);
                        console.log(companies[0]);
                        callAccountGraphQL(companies[0].id);
                    },
                    error: function() { 
                        console.log(data);
                    }
                });
            };
            function callWorkerGraphQL()
            {
                $.ajax({
                    type: 'POST', 
                    url: '/graphql',
                    data:{query:`query{workers{id,name}}`},
                    success: function (data) {
                        console.log(data);
                        var workers = data.data.workers;
                        $('#workers').html("");
                        html="";
                        for (i = 0; i < workers.length; i++) {
                            worker = workers[i];
                            html+=`<option value=${worker.id}>${worker.name}</option>`;
                        }
                        $('#workers').html(html);
                    },
                    error: function() { 
                        console.log(data);
                    }
                });
            };
            function callAccountGraphQL(value)
            {
                $.ajax({
                    type: 'POST', 
                    url: '/graphql',
                    data:{query:`query{accounts(where:{column:COMPANY_ID,value:${value}}){id,name}}`},
                    success: function (data) {
                        var accounts = data.data.accounts;
                        
                        $('#accounts').html("");
                        html="";
                        for (i = 0; i < accounts.length; i++) {
                            account = accounts[i];
                            html+=`<option value=${account.id}>${account.name}</option>`;
                        }
                        $('#accounts').html(html);
                    },
                    error: function() { 
                        console.log(data);
                    }
                });
            };
            function callTransactionsGraphQL()
            {
                companies = $('#companies').val();
                workers = $('#workers').val();
                accounts = $('#accounts').val();
                if(companies == null || workers == null || accounts== null){
                    $('#pdf').hide();
                    $('#excel').hide();
                    return;
                }
                datepicker_from = $('#datepicker_from').val();
                datepicker_to = $('#datepicker_to').val();

                open = "";
                closure = "";

                datepicker_from_q = "";
                datepicker_to_q = "";
                if(datepicker_from != ""){
                    open = "(";
                    closure=")";
                    datepicker_from_q = `from:"${datepicker_from} 00:00:00"`; 
                }
                if(datepicker_to != ""){
                    open = "(";
                    closure=")";
                    datepicker_to_q = `to:"${datepicker_to} 00:00:00"`;
                }
                
                
                workers_q = "";
                companies_q = "";
                accounts_q = "";

                $('#workers_check').val();
                $('#companies_check').val();
                $('#accounts_check').val();
                if(!$('#workers_check').is(":checked")){
                    open = "(";
                    closure=")";
                    workers_q = `worker_id:${workers}`;
                }
                if(!$('#companies_check').is(":checked")){
                    open = "(";
                    closure=")";
                    companies_q = `company_id:${companies}`;
                }
                if(!$('#accounts_check').is(":checked")){
                    open = "(";
                    closure=")";
                    accounts_q = `account_id:${accounts}`;
                }

                $.ajax({
                    type: 'POST', 
                    url: '/graphql',
                    data:{query:`query{summary${open} ${workers_q} ${companies_q} ${accounts_q} ${datepicker_from_q} ${datepicker_to_q}${closure}{id amount currency{name} company{name} account{name} worker{name} exchange_rate amount_egp created_at direction}}`},
                    success: function (data) {
                        console.log(data);
                        var transactions = data.data.summary;
                        
                        $('#transactions').html("");
                        html="";
                        ids=new Array();
                        $('#pdf').show();
                        $('#excel').show();
                        if(transactions.length == 0){
                            html+= `<tr> <td colspan=8> No Data Found</tr> </td>`;
                            $('#pdf').hide();
                            $('#excel').hide();
                        }

                        for (i = 0; i < transactions.length; i++) {
                            transaction = transactions[i];
                            ids.push(transaction.id);
                            html+=`<tr><td>${transaction.amount}</td><td>${transaction.currency.name}</td><td>${transaction.company.name}</td><td>${transaction.account.name}</td><td>${transaction.worker.name}</td><td>${transaction.exchange_rate}</td>`;

                            if(transaction.direction == "OUT"){
                                html+=`<td style="color:red;">-${transaction.amount_egp}</td>`;
                            }
                            else{
                                html+=`<td style="color:green;">${transaction.amount_egp}</td>`;
                            }
                            html+=`<td>${transaction.created_at}</td></tr>`;
                        }

                        $('#transactions').html(html);

                        $('#pdfdata').val(JSON.stringify(ids));
                        $('#exceldata').val(JSON.stringify(ids));
                    },
                    error: function() { 
                        console.log(data);
                    }
                });
            };
        </script>
    </body>
</html>
