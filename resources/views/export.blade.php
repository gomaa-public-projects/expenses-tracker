<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        
        <style>
            td{
                width:20%;
            }
            html{
                background-color: #fff;
                color: #636b6f;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            body{
                font-family: Helvetica;
            }
            /* Table Styles */

            .head-wrapper{
                margin: 50px 70px 50px;
                font-size:12px;
                font-weight: bold;
            }
            .highlight{
                font-weight: normal;
            }
            .table-wrapper{
                margin: 10px 70px 70px;
            }

            .fl-table {
                border-radius: 5px;
                font-size: 12px;
                font-weight: normal;
                border: none;
                border-collapse: collapse;
                width: 100%;
                max-width: 100%;
                white-space: nowrap;
                background-color: white;
            }

            .fl-table td, .fl-table th {
                text-align: center;
                padding: 8px;
            }

            .fl-table td {
                border-right: 1px solid #f8f8f8;
                font-size: 12px;
            }

            .fl-table thead th {
                color: #ffffff;
                background: #4FC3A1;
            }


            .fl-table thead th:nth-child(odd) {
                color: #ffffff;
                background: #324960;
            }

            .fl-table tr:nth-child(even) {
                background: #F8F8F8;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <h1 style="text-align:center">Financial Report</h1>
                <div class="head-wrapper">
                    <p><span class="highlight">Report From:</span>  {{$from}} <span class="highlight"> &emsp; To:</span> {{$to}}</p>
                    <p><span class="highlight">Issued By:</span> {{$name}}</p>
                    <p><span class="highlight">Date and Time:</span> {{$now}}</p>
                </div>
                <div id="exportpdf" class="table-wrapper">
                    <table class="fl-table">
                        <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Currency</th>
                            <th>Company</th>
                            <th>Account</th>
                            <th>Worker</th>
                            <th>Exchange Rate</th>
                            <th>Amount (EGP)</th>
                            <th>Creation Date</th>
                        </tr>
                        </thead>
                        <tbody id="transactions">
                            @foreach($data as $transaction)
                                <tr>
                                    <td>{{number_format($transaction->amount, 2)}}</td>
                                    <td>{{$transaction->currency->name}}</td>
                                    <td>{{$transaction->company->name}}</td>
                                    <td>{{$transaction->account->name}}</td>
                                    <td>{{$transaction->worker->name}}</td>
                                    <td>{{number_format($transaction->exchange_rate, 2)}}</td>
                                    @if($transaction->direction == "out")
                                        <td style="color:red;">-{{number_format($transaction->amount_egp, 2)}}</td>
                                    @else
                                        <td style="color:green;">{{number_format($transaction->amount_egp, 2)}}</td>
                                    @endif
                                    <td>{{$transaction->created_at}}</td>
                                </tr>
                            @endforeach
                        <tbody>
                    </table>
                </div>
                <div id="exportpdf_2" class="table-wrapper">
                    <table class="fl-table">
                        <tr> 
                            <td colspan="4" style="background-color:#4FC3A1; font-size:15px; color:white;"> Total: </td>
                            <td colspan="4" style="background-color:#324960; font-size:15px; color:white;">{{$total}} (EGP)</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
