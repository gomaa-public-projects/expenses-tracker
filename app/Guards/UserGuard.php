<?php

namespace App\Guards;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Exceptions\LightHouseCustomException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use App\AuthUser;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Log;


class UserGuard implements Guard
{
    //use GuardHelpers;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * The name of the query string item from the request containing the API token.
     *
     * @var string
     */
    protected $inputKey;

    /**
     * The name of the token "column" in persistent storage.
     *
     * @var string
     */
    protected $storageKey;

    /**
     * Indicates if the API token is hashed in storage.
     *
     * @var bool
     */
    protected $hash = false;

    public function __construct($provider)
    {
        $this->request = request();
        $this->provider = $provider;
    }

    public function check()
    {
    }

    public function guest()
    {
    }

    public function user()
    {

        $token = $this->request->bearerToken();
        if (empty($token)) {
            Log::channel('guards')->info("UserGuard: Token is Missing");
            throw new LightHouseCustomException(401, 'Token is Missing!!');
        }
        
        $AuthUser = $this->retrieveByCredentials(array('token' => $token));
        try {
            $myUser = $this->provider->retrieveById($AuthUser->id);
            Log::channel('guards')->info("UserGuard: returning Customer of id {$myUser->id}");
            Log::channel('guards')->info("UserGuard: guard has finished");
            return $this->user = $myUser;
        } catch (ModelNotFoundException $e) {
            Log::channel('guards')->info("UserGuard: returning NULL");
            Log::channel('guards')->info("UserGuard: guard has finished");
            return NULL;
        }
    }

    public function id()
    {
    }

    public function validate(array $credentials = [])
    {
        return $this->provider->validate($credentials);
    }

    public function attempt(array $credentials = [])
    {
        $this->validate($credentials);
        return $this->retrieveByCredentials($credentials);
    }

    public function setUser(Authenticatable $user)
    {
    }

    public function retrieveByCredentials(array $credentials)
    {
        return $this->provider->retrieveByCredentials($credentials);
    }
}
