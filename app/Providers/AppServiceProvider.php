<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Worker;
use App\Transaction;
use App\Observers\WorkerObserver;
use App\Observers\TransactionObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Transaction::observe(TransactionObserver::class);
        Worker::observe(WorkerObserver::class);
    }
}
