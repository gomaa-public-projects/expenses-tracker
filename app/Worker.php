<?php

namespace App;

use Akwad\VoyagerExtension\Facades\Voyager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
class Worker extends \Akwad\VoyagerExtension\Models\User
{
    protected $fillable = [
        'handle',
        'password',
        'name',
        'auth_user_id',
        'position',
    ];
    public function authUser(): BelongsTo
    {
        return $this->belongsTo(AuthUser::class);
    }
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class);
    }
    public function managementNodes(): BelongsToMany
    {
        return $this->belongsToMany(ManagementNode::class);
    }
    public function getHandleAttribute()
    {
        if(isset($this->authUser)){
            return $this->authUser->handle;
        }
        return NULL;
    }
    public function getRoleIdAttribute($value)
    {
        return Voyager::modelClass('Role')::where('name',$this->position)->first()->id;
    }

    public function scopeAuthorizedWorkersOnly($query)
    {
        return $query->where('position', '=', 'worker');
    }
    public function scopeHideMasters($query)
    {
        return $query->where('position', '!=', 'master')->orWhere('id',Auth()->user()->id);
    }
}
