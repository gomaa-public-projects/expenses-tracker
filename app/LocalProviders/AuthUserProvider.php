<?php

namespace App\LocalProviders;

use App\AuthUser;
use \Firebase\JWT\JWT;
use \DomainException;
use \InvalidArgumentException;
use \UnexpectedValueException;
use App\WhiteListToken;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Akwad\VoyagerExtension\Models\Role;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use App\Exceptions\LightHouseCustomException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthUserProvider implements UserProvider
{
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */

    private $role;

    public function __construct($role = null)
    {
        $this->role = $role;
    }
    public function retrieveById($identifier)
    {
        $role = $this->role?? AuthUser::where('id',$identifier)->firstOrFail()->role;
        $model = Role::where('name', $role)->firstOrFail()->model;
        return $model::where('auth_user_id', $identifier)->firstOrFail();
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return NULL;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        return NULL;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if(isset($credentials['token'])){
            $token = $credentials['token'];
            
            $secretKey = config("authentication.JWT_SECRET");
            $secretKey = base64_decode($secretKey);
    
            $flag = true;
            try {
                $white_Listed = WhiteListToken::where('token', $token)->firstOrFail();
            } catch (ModelNotFoundException $e) {
                Log::channel('guards')->info("UserGuard: Token is BlackListed");
                throw new LightHouseCustomException(1400, 'Token is BlackListed!!');
            }
    
            try {
                
                JWT::$leeway = 60 * 60 * 24; //24 hour
                $data = JWT::decode($token, $secretKey, ['HS512']);
                return AuthUser::find($data->data->id);

            } catch (DomainException | InvalidArgumentException | UnexpectedValueException  $e) {
                Log::channel('guards')->info("UserGuard: Token is Invalid");
                throw new LightHouseCustomException(601, 'Token is Invalid!!');
            }
        }
        if(isset($credentials['handle'])){
            return AuthUser::where('handle', $credentials['handle'])->firstOrFail();
        }
        return AuthUser::where('handle', $credentials['email'])->firstOrFail();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $handle = str_replace(" ","",$credentials['email']);
        if(strtolower($user->handle) == strtolower($handle) && Hash::check($credentials['password'],$user->password)){
            return true;
        }
        return false;
    }

    public function validate($credentials){
        $user = AuthUser::where('handle', $credentials['handle'])->first();
        if (!Hash::check($credentials['password'], $user->password)) {
            Log::channel('guards')->info("UserGuard (validate): Wrong Credintials");
            throw new LightHouseCustomException(403, __('wrong_creds'));
        }
        // if ($user->email_verified_at == null) {
        //     Log::channel('guards')->info("UserGuard (validate): User email hasn't been verified yet");
        //     throw new LightHouseCustomException(405, "User email hasn't been verified yet");
        // }
    }

    public function getModel(){
        return NULL;
    }
}
