<?php

namespace App\Observers;

use App\Currency;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Exceptions\LightHouseCustomException;

class TransactionObserver
{
    public function creating(Transaction $transaction)
    {
        if(isset($transaction->getAttributes()['currency_id'])){
            $currency = Currency::find($transaction->getAttributes()['currency_id']);
            $transaction->amount_egp = $transaction->amount * $currency->exchange_rate;
            $transaction->exchange_rate = $currency->exchange_rate;
        }
    }
}
