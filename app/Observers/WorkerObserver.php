<?php

namespace App\Observers;

use App\Worker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Exceptions\LightHouseCustomException;

class WorkerObserver
{
    public function updating(Worker $worker)
    {
        $changes = $worker->getDirty();
        if(isset($changes['password']) && $worker->password){
            if(isset($worker->authUser)){
                $worker->authUser->update(['password' => $worker->password]);
            }
        }
    }
}
