<?php

namespace App\Listeners;

use Nuwave\Lighthouse\Events\StartRequest;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class GraphQLQueryListener 
{
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';
    public $timeout = 10;
    public $actor_id,$actor_type;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param   $event
     * @return void
     */
    public function handle(StartRequest $event)
    {
        $result = $event->request->query();
        if(strpos($result, 'query IntrospectionQuery')!== false){
            return;
        }
        if(strpos($result, 'mutation')!== false){
            Log::channel('graphQL_Queries')->info("------------- Graph QL Mutation -------------");
            Log::channel('graphQL_Queries')->info($event->request->query());
            Log::channel('graphQL_Queries')->info("-------------- End of Mutation --------------");
        }
        else{
            Log::channel('graphQL_Queries')->info("--------------- Graph QL Query --------------");
            Log::channel('graphQL_Queries')->info($event->request->query());
            Log::channel('graphQL_Queries')->info("---------------- End of Query ---------------");
        }
    }


    /**
     * Handle a job failure.
     *
     * @param  \App\Events\OrderShipped  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(StartRequest $event, $exception)
    {
        //
    }
}
