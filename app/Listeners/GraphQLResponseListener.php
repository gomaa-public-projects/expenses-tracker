<?php

namespace App\Listeners;

use Nuwave\Lighthouse\Events\ManipulateResult;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class GraphQLResponseListener 
{
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'listeners';
    public $timeout = 10;
    public $actor_id,$actor_type;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param   $event
     * @return void
     */
    public function handle(ManipulateResult $event)
    {
        Log::channel('graphQL_Queries')->info("------------- Graph QL Response -------------");
        if($event->result->errors){
            foreach($event->result->errors as $error){
                Log::channel('graphQL_Queries')->error($error->message);
            }
        }
        else if(strpos(json_encode($event->result->data), '__schema')!== false){
            return;
        }
        else{
            Log::channel('graphQL_Queries')->info(json_encode($event->result->data));
        }
        Log::channel('graphQL_Queries')->info("-------------- End Of Response --------------"); 
    }


    /**
     * Handle a job failure.
     *
     * @param  \App\Events\OrderShipped  $event
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(ManipulateResult $event, $exception)
    {
        //
    }
}
