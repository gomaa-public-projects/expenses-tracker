<?php

namespace App\Listeners;

use App\AuthUser;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class CreateNewWorker
{

    public $queue = 'listeners';
    public $timeout = 10;

    public function __construct()
    {
        //
    }

    public function handle(Worker $worker)
    {
        Log::channel('listeners')->info("CreateNewWorker: listener in progress");
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        $password = substr($random, 0, 12);
        $myauth = AuthUser::create([
            'type' => 'handle',
            'handle' => $worker->getAttributes()['handle'],
            'password' => $worker->getAttributes()['password'],
            'role' => 'worker',
            'email_verified_at' => Carbon::now(),
        ]);

        Log::channel('listeners')->info("CreateNewWorker: auth user {$myauth->id} created");
        $worker->auth_user_id = $myauth->id;
        $worker->password = null;
        $worker->handle = null;
        Log::channel('listeners')->info("CreateNewWorker: listener has finished");
    }

}
