<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Models\Role;

class SuperAdminAuth
{
    protected $allowed_roles = ['admin'];
    public function handle($request, Closure $next)
    {

        if(!Auth::guard('MyVoyagerGuard')->user()){
            return redirect('admin/login');
        }

        $name = Auth::guard('MyVoyagerGuard')->user()->authUser->role;
        if(!in_array($name, $this->allowed_roles)){
            return redirect('admin');
        }

        return $next($request);
    }
}
