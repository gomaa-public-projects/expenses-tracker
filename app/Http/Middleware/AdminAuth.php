<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Akwad\VoyagerExtension\Models\Role;

class AdminAuth
{
    protected $allowed_roles = ['admin','user','master','assistant'];
    public function handle($request, Closure $next)
    {

        if(!Auth::guard('MyVoyagerGuard')->user()){
            return redirect('admin/login');
        }

        $name = Auth::guard('MyVoyagerGuard')->user()->authUser->role;
        if(!in_array($name, $this->allowed_roles) && !in_array(Auth::guard('MyVoyagerGuard')->user()->position, $this->allowed_roles)){
            return redirect('admin');
        }

        return $next($request);
    }
}
