<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade as PDF;
use App\Transaction;
use Illuminate\Support\Facades\Auth;

class ExportController extends Controller
{
    public function pdf(){
        $pdf = App::make('dompdf.wrapper');
        $json = json_decode(request()->pdfdata);

        $data = Transaction::whereIn('id',$json)->get();
        $total = ceil ($data->where('direction','in')->sum('amount_egp') - $data->where('direction','out')->sum('amount_egp'));
        $transactions = Transaction::whereIn('id',$json)->orderBy('created_at','asc')->get();
        $from = Carbon::createFromFormat('Y-m-d H:i:s',$transactions->first()->created_at)->format('Y-m-d');
        $to = Carbon::createFromFormat('Y-m-d H:i:s',$transactions->last()->created_at)->format('Y-m-d');

        $now = Carbon::now()->setTimezone(config("app.TIMEZONE"))->format('Y-m-d g:i:s A');

        $name ="";

        $user = Auth::guard('MyVoyagerGuard')->user();

        if($user){
            $name = $user->name;
        }

        //return view('export', ['data' =>$data, 'now' => $now, 'from' => $from, 'to' => $to, 'name' => $name, 'total' => $total]);
        $pdf = PDF::loadView('export', ['data' =>$data, 'now' => $now, 'from' => $from, 'to' => $to, 'name' => $name, 'total' => $total]);
        return $pdf->download('invoice.pdf');
    }

    public function excel(){
        $json = json_decode(request()->exceldata);
        $transactions = Transaction::whereIn('id',$json)->get();

        $rows = array();

        $headers = array(
            'Amount',
            'Currency',
            'Company',
            'Account',
            'Worker',
            'Exchange Rate',
            'Amount EGP',
            'Creation Date',
        );

        $rows[] = $headers;

        foreach($transactions as $transaction){
            $row = array(
                number_format($transaction->amount, 2),
                $transaction->currency->name,
                $transaction->company->name,
                $transaction->account->name,
                $transaction->worker->name,
                number_format($transaction->exchange_rate, 2),
                number_format($transaction->amount_egp, 2),
                $transaction->created_at
            );
            $rows[] = $row;
        }

        return (collect($rows))->downloadExcel('reports.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
}
