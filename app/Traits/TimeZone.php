<?php

namespace App\Traits;

use Carbon\Carbon;

trait TimeZone
{
    public function shiftDate($value)
    {
        if($value == NUll){
            return $value;
        }
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $value, 'UTC');
        $date->setTimezone(config("app.TIMEZONE"));
        return $date;
    }
    public function unShiftDate($value)
    {
        if($value == NUll){
            return $value;
        }
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $value, config("app.TIMEZONE"));
        $date->setTimezone('UTC');
        return $date;
    }
}
