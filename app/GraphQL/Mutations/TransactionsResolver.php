<?php

namespace App\GraphQL\Mutations;

use DB;
use Carbon\Carbon;
use App\Transaction;
use Illuminate\Support\Facades\Log;
use GraphQL\Type\Definition\ResolveInfo;
use App\Exceptions\LightHouseCustomException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;



class TransactionsResolver
{

    public function balance($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return ceil ($this->calculate($args)->where('direction','in')->sum('amount_egp') - $this->calculate($args)->where('direction','out')->sum('amount_egp'));
    }

    public function summary($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return $this->calculate($args)->get();
    }

    public function calculate($args){
        $transactions = Transaction::whereNotNull('created_at');

        if(isset($args['worker_id'])){
            $transactions->where('worker_id',$args['worker_id']);
        }
        if(isset($args['company_id'])){
            $transactions->where('company_id',$args['company_id']);
        }
        if(isset($args['account_id'])){
            $transactions->where('account_id',$args['account_id']);
        }

        if(isset($args['today']) && $args['today']){
            $transactions->whereDate('created_at','=',Carbon::now()->toDateString());
        }
        else if(isset($args['from']) && isset($args['to'])){
            $transactions->whereDate('created_at','>=',$args['from']->toDateString())->whereDate('created_at','<=',$args['to']->toDateString()); 
        }
        else if(isset($args['from'])){
            $transactions->whereDate('created_at','>=',$args['from']->toDateString()); 
        }
        else if(isset($args['to'])){
            $transactions->whereDate('created_at','<=',$args['to']->toDateString()); 
        }
        return $transactions;
    }
}