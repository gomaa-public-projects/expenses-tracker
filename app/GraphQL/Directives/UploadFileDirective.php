<?php

namespace App\GraphQL\Directives;

use Nuwave\Lighthouse\Support\Contracts\ArgTransformerDirective;
use Nuwave\Lighthouse\Support\Contracts\Directive;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Exceptions\LightHouseCustomException;
use Nuwave\Lighthouse\Support\Contracts\DefinedDirective;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UploadFileDirective extends BaseDirective implements Directive,DefinedDirective,ArgTransformerDirective
{
    public $max_size = 2*1024*1024;//2MB
    public $uploadPath = "GraphQL";
    public $fieldName;
    /**
     * Directive name.
     *
     * @return string 
     */
    public function name(): string
    {
        return 'uploadFile';
    }

    public static function definition(): string
    {
        return /* @lang GraphQL */ <<<'SDL'
directive @uploadFile(
  type: String
  storage: String!
  path: String
  max_size: Int
) on FIELD_DEFINITION | INPUT_FIELD_DEFINITION
SDL;
    }
 
    public function transform($argumentValue)
    {
        $this->fieldName = $this->definitionNode->name->value;

        if($argumentValue==NULL)
       {
        return NULL;
       } 
       else if(strpos($argumentValue, 'GraphQL') !== false)
       {
            try
            {
                $detach= strstr($argumentValue, 'GraphQL',true);
                $argumentValue=str_replace ($detach,"",$argumentValue);
            }
            catch (ModelNotFoundException $e)
            {
                throw new LightHouseCustomException(406,'Submitted '.$this->fieldName.' value is not correct');
            } 
            return $argumentValue;
       }
       else if(strpos($argumentValue, 'upload') !== false)
       {
            try
            {
                $detach= strstr($argumentValue, 'upload',true);
                $argumentValue=str_replace ($detach,"",$argumentValue);
            }
            catch (ModelNotFoundException $e)
            {
                throw new LightHouseCustomException(406,'Submitted '.$this->fieldName.' value is not correct');
            } 
            return $argumentValue;
       }
       else
       {
        $type=$this->directiveArgValue('type');
        $storage=$this->directiveArgValue('storage');
        $path=$this->directiveArgValue('path');
        $types = explode("|", $type);

        return $this->upload($argumentValue,$storage,$types,$path);
       }
    }

    public function upload($file,$storage,$types,$path){
            
        $file_name="File_".now()->format('y-m-d-h-i-s')."_".rand(1,9999);//Random Name
        $tempFilePath = 'tempFiles/temp_' .$file_name;// Add Temp File to temp files

        Storage::disk('public')->put($tempFilePath, base64_decode($file));//Storing temp File
        $fullFilePath = Storage::disk('public')->path($tempFilePath);
        $tempFile=new File($fullFilePath);

        if(!in_array($tempFile->extension(), $types))//check if image type supported
        { 
            $myType=$tempFile->extension();
            Storage::disk('public')->delete($tempFilePath);
            throw new LightHouseCustomException(406,'at '.$this->fieldName.' Field, ['. $myType.'] Type not Supported!!');
        }

        if($this->directiveArgValue('max_size')!=NULL)
        {
         $this->max_size =$this->directiveArgValue('max_size')*1024;
        }

        if($tempFile->getSize()>($this->max_size))//check if file size allowed
        { 
            Storage::disk('public')->delete($tempFilePath);
            throw new LightHouseCustomException(406,'at '.$this->fieldName.' Field, File size is Too Large!!');
        }

       
        if($path!=NULL){
            $this->uploadPath.="/".$path;
        }

        $fileLink=Storage::disk($storage)->putFileAs($this->uploadPath, $tempFile,$file_name.".".$tempFile->extension(),'public');
        
        Storage::disk('public')->delete($tempFilePath);
        return $fileLink;   
    }

}
