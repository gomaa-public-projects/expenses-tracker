<?php

namespace App\Policies;

use App\AuthUser;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;


    public function viewAny(?AuthUser $user)
    {
        //
    }


    public function view(?AuthUser $user, $userProfile)
    {
        //
    }


    public function create(?AuthUser $user, $input)
    {
        //
    }


    public function update(?AuthUser $user, $userProfile)
    {
        if (Auth::guard('UserGuard')->user() == NULL) {
            return false;
        }
        return Auth::guard('UserGuard')->user()->auth_user_id == $userProfile->auth_user_id;
    }


    public function delete(?AuthUser $user, $userProfile)
    {
        //
    }


    public function restore(?AuthUser $user,  $userProfile)
    {
        //
    }


    public function forceDelete(?AuthUser $user,  $userProfile)
    {
        //
    }
}
