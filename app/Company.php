<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Company extends Model
{
    protected $fillable = [
        'name',
        'logo',
    ];

    public function getLogoAttribute($value)
    {
        if (!request()->is('graphql'))
        {
            return $value;
        }
       
        if ($value == null) {return $value;}
        return asset('storage/' . $value);
    }
    public function workers(): BelongsToMany
    {
        return $this->belongsToMany(Worker::class);
    }
    public function accounts(): HasMany
    {
        return $this->hasMany(Account::class);
    }
    public function managedWorkers($rootValue)
    {
        $manager = Auth::guard('UserGuard')->user();
        if(!$manager){
            return [];
        }
        return Worker::whereHas('managementNodes', function ($q) use($rootValue, $manager) {
            $q->whereHas('company', function ($q)  use($rootValue) {
                $q->where('company_id', $rootValue->id);
            })->where('manager_id',$manager->id);
        })
        ->get();
    }
    public function managementNodes(): HasMany
    {
        return $this->hasMany(ManagementNode::class);
    }
}
