<?php

namespace App;
//use App\Traits\TimeZone;

use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\Notifiable;

class AuthUser extends User
{
    use Notifiable;
    //use TimeZone;


    protected $fillable = [
        'type',
        'email',
        'password',
        'token',
        'role',
        'locale',
        'fcm_token',
        'email_verified_at',
        'last_logged_in',
        'remember_token',
        'handle',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFirstLoggedAttribute()
    {
        if ($this->last_logged_in != null) {
            return true;
        }
        return false;
    }
}
