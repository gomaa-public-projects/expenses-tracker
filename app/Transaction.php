<?php

namespace App;

use Carbon\Carbon;
use App\Traits\TimeZone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Transaction extends Model
{
    use TimeZone;
    protected $fillable = [
        'amount',
        'amount_egp',
        'exchange_rate',
        'currency_id',
        'company_id',
        'account_id',
        'worker_id',
        'notes',
        'direction',
        'attachments',
    ];

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }
    public function worker(): BelongsTo
    {
        return $this->belongsTo(Worker::class);
    }
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }
    public function getCreatedAtAttribute($value){
        return $this->shiftDate($value);
    }
    
    public function transactionFiles(): HasMany
    {
        return $this->hasMany(TransactionFile::class);
    }
    public function getAttachmentsAttribute($value){
        if(!$value && !$this->transactionFiles->first()){
            return null;
        }

        if(!$value){
            $link = $this->CreateAttachments();
            $this->update(['attachments' => $link]);

            return $this->fileParser($link);
        }

        return $this->fileParser($value);
    }

    public function CreateAttachments(){
        $files = array();
        $folder_name = 'folder_'.$this->id."_".Carbon::now()->format('h_i_s');
        $zip_name = 'zip_'.$this->id."_".Carbon::now()->format('h_i_s').".zip";
        mkdir(storage_path('app/public')."/"."GraphQL/files/".$folder_name , 0775);
        
        foreach ($this->transactionFiles as $transactionFile){
            $links = explode("/",$transactionFile->file_link);
            $file_link = end($links);
            
            rename(storage_path('app/public')."/".$transactionFile->file_link, storage_path('app/public')."/"."GraphQL/files/".$folder_name."/".$file_link);
            $transactionFile->update(["file_link" => "GraphQL/files/".$folder_name."/".$file_link]);
        }
        $zipper = new \Madnest\Madzipper\Madzipper;
        $zip = $zipper->make(storage_path('app/public')."/zip_files".'/'.$zip_name)->add(storage_path('app/public').'/GraphQL/files/'.$folder_name)->close();
        return "zip_files/".$zip_name;
    }

    public function fileParser($value){
        if ($value == null) {
            return $value;
        }
        if (!request()->is('graphql')) {
            $meta_data = new \stdClass;
            $meta_data->download_link = $value;
            $meta_data->original_name = 'Download Attachments';
            $value = array();
            $value[0] = $meta_data;
            return json_encode($value);
        }

        return asset('storage/' . $value);
    }
}
